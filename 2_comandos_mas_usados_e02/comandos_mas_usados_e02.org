#+TITLE: Primeros comandos de Bash en GnuLinux, parte II
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:t num:t
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col)


* Temario de la Clase

- La lógica del sistema POSIX ::
Aprenderemos las formas generales de como funciona un sistema de comandos como
~bash~ en GnuLinux, esta es una definición estandarizada llamada ~POSIX~.

 - Comandos para manipular directorios ::
Revisaremos los comandos que, desde la terminal, nos permitan crear, modificar y
eliminar directorios.

* Registrar Inicio de Clases
** Una marca en la línea de comandos

Todas las clases deberemos registrar el inicio de clases para resguardar y
tener almacenadas nuestras acciones.

Usaremos el comando ~echo~ para dejar una marca

#+BEGIN_SRC bash
echo "CLASE_02"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_01*

También, otra forma de controlar el inicio de clases es anotando el último
número que aparece a la izquierda.

#+BEGIN_SRC bash
514  git status
515  git add .
516  git commit -m "archivos en directorios de ej 1"
517  git push
#+END_SRC

En este caso, el último comando antes de comenzar nuestra nueva clase es *517*

* Manipulación de directorios

** Crear 

Para crear carpetas (directorios) en un punto, tienes dos opciones:

- Conocer la ruta completa en el punto de creación
- Moverte hasta el directorio padre y crear la carpeta.

#+BEGIN_SRC bash
mkdir NUEVA_CARPETA
#+END_SRC

Como ejercicio, en *raiz* crear las siguientes carpetas:

- rama_a3
- rama_a3/rama_a31
- rama_a2/rama_a32
- rama_a4
- rama_b/rama_b2/rama_b22

** Mover o cambiar

Tienes que tener la noción de *origen* y *destino*.

- ¿Cúal es la carpeta origen? :: la que quieres mover
- ¿Cúal es la carpeta destino? :: el nuevo nombre de la carpeta

#+BEGIN_SRC bash
mv ORIGEN DESTINO
#+END_SRC

Como ejercicio mover

- origen :: rama_a4
- destino :: rama_b2/rama_b22

** Eliminar

Para eliminar directorios (y de manera recursiva, todo lo que hay adentro de
uno), será necesario usar la opción ~-r~.

#+BEGIN_SRC bash
rm -r directorio
#+END_SRc

Como ejercicio, borrar la *rama_b2*

Opciones:

- ubica la ruta completa a *rama_b2*
- muévete hasta la /rama madre de/ *rama_b2*

#+BEGIN_SRC bash
rm -r ruta_rama_b2
#+END_SRc

* Buscar directorios o archivos

** Uso de /find/

Para buscar dentro de un directorio y todas sus ramas, el comando más útil será:

#+BEGIN_SRC bash
find -iname "archivo_semilla"
#+END_SRC

La opción *iname* permite buscar por coincidencias sin importar si el caracter
está en minúsculas o mayúsculas.

Buscar, desde raíz, los siguientes archivos o directorios

- archivo_semilla
- archivo_semilla*
- rama_a13

¿Qué resultados da? Anótalos en tus apuntes.

* Generalidades de la terminal bash

** Esquema de escritura de un comando

En general un comando de la terminal necesita una entrada, que puede tomar una por defecto si no se le da alguna, y 
los argumentos opcionales que permiten controlar cómo mostrar los resultados. 


#+CAPTION: Estructura general de un comando
#+NAME:   fig: comando
#+ATTR_HTML: width="200px"
#+ATTR_ORG: :width 200
#+attr_latex: :width 200px

[[file:./img/comando_gen.png]]

** Reflexión sobre la estructura de un comando

¿Cuándo usamos los siguientes comandos, a qué corresponde cada parte?

En la terminal, en tu carpeta de ejercicios personal de la ~clase_02~.

- cd listar
- ls -la
- ls -l *.png
- touch nuevo_archivo
- mv nuevo_archivo super_archivo
- mkdir nueva_carpeta
- mkdir -p nueva_carpeta/chiquitita/absurda

** Uso de la carpeta ejercicios/listar

Para comprender el uso de la terminal bash y sus características, usaremos,
dentro de la carpeta *./ejercicios/listar* los comandos que veamos a continuación.

** Esquema de salidas frente a un comando

Existen dos salidas frente a la ejecución de un comando

- stdout :: salida estándar o, lo que uno espera recibir

- stderr :: salida del error, si es que algo falla

#+CAPTION: Estructura general de un comando
#+NAME:   fig: comando
#+ATTR_HTML: width="150px"
#+ATTR_ORG: :width 150
#+attr_latex: :width 150px
[[file:./img/outputs.png]]
Lo que resulta de un comando, entonces, puede ser una salida *estándar* o bien
un *error*, pero ambas salidas son un *stream*.


** Encadenamiento de comandos con la pleca '|'

En *bash* se puede realizar una serie de acciones encadenadas o /'empipadas'/. El signo *|* permite
que una salida o respuesta a una acción o comando pueda ser tomada como entrada del siguiente comando.

Por ejemplo, si uno busca los archivos con extensión ~*.png~ cuyo nombre contenga una /a/.

#+BEGIN_SRC bash
ls -lsah *.png | grep a
#+END_SRC

** Salida a archivo, escribir nuevo o añadir nuevas líneas

Si en vez de querer ver la salida en la terminal, la deseas guardar en un archivo, es simple, utilizando
*>* o bien *>>* encadenadas al final del comando 

#+BEGIN_SRC bash
ls -lsah *.png | grep a > archivo_salida.txt
ls -lsah *.png | grep a >> archivo_salida.txt
#+END_SRC

*** Tarea para la casa

Investigar como se debe hacer para capturar en un archivo solamente 

- la salida estándar (los resultados correctos)
- la salida de error

Puede preguntar a los ayudantes.

* Resguardar los comandos realizados

** Guardar los comandos realizados en clases

Cada sesión realizaremos una serie de comandos, estos se almacenan en memoria.

En cada clase haz lo siguiente, dónde *NUMERO* es el número de la clase:

#+BEGIN_SRC bash
history
echo "CLASE NUMERO"
#+END_SRC

Al finalizar guarda los comandos realizados en un archivo. (es un comando que
veremos a futuro, pero úsalo). Crea antes la carpeta *historia*, en dónde te acomode.


- ¿Cuál era la seña que anotaste al principio?

#+BEGIN_SRC bash
history | grep -A 1000 "CLASE_02" > 
historia/clase_numero.sh
#+END_SRC

* Crear un script básico

** Crear un script muy sencillo

Crear una carpeta *scripts*

Crear un archivo *hola_mundo.sh*

Ponerle el siguiente contenido al archivo: 

#+BEGIN_SRC  bash
echo "Hola mundo!"
#+END_SRC

** Correr un script usando el intérprete

Correr el script:

#+BEGIN_SRC  bash
bash hola_mundo.sh
#+END_SRC

** Crear un script que consulte el nombre

En su carpeta de trabajo personal, en la ~clase_02/scripts~.

Utilizando el comando ~read~ (ver el manual), escriba un script que pregunte el
nombre y le responda con:


#+BEGIN_SRC  bash
echo "¿Cuál es tu nombre?"
read NOMBRE
echo "Hola $NOMBRE, bienvenida/o 
al mundo de la programación!"
#+END_SRC

- ejemplos de uso :: https://www.computerhope.com/unix/bash/read.htm

* Cerrando la clase
** Tarea para próxima clase

La próxima clase consiste en trabajar con el comando ~git~, con el que
aprenderemos a gestionar las versiones de los proyectos y resguardar los
avances.

Vamos a hacer una clase de literatura con *git*. Para eso busca los siguientes
textos:

- tus 3 poemas preferidos
- tus 3 cuentos preferidos
- escoge un cuento de Horacio Quiroga, Julio Cortazar o Manuel Rojas.
- escoge 2 noticias que consideres importantes
- selecciona un ensayo de un autor/autora europeo/a
- selecciona un ensayo de un autor/autora latinoamericano/a
- selecciona 3 obras de artes visuales que te gusten
- haz un pequeño resumen de cada texto u obra

** Fin de clase

¡Bienvenido/a al mundo de la programación!

~Guarda tus comandos :)~

