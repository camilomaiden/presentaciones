(TeX-add-style-hook
 "6_edicion_sed"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "aspectratio=169")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "babel"
    "setspace")
   (LaTeX-add-labels
    "sec:org1a062c8"
    "sec:orgd7a39f0"
    "sec:org23c02d2"
    "sec:orgcdb8032"
    "sec:org8a0f8b5"
    "sec:org05077f8"
    "sec:org3794639"
    "sec:orgd4d7857"
    "sec:org3060ac1"
    "sec:orgf381333"
    "sec:org85d2ad8"
    "sec:org8c29986"
    "sec:org4db5c40"
    "sec:org7bc0187"
    "sec:orgcc75254"
    "orgf5f4479"
    "sec:orga48f6ae")
   (LaTeX-add-xcolor-definecolors
    "links"))
 :latex)

