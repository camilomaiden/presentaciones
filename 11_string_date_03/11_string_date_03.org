#+TITLE: Cadenas de texto, transformaciones y formatos III
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+BEAMER_HEADER: \setbeamertemplate{navigation symbols}{}
#+COLUMNS: %5ITEM %10BEAMER_ENV(Env) %5BEAMER_ACT(Act) %6BEAMER_COL(Col)
#+latex_class_options: [10pt]

* Temario de la clase

** ¿Qué veremos en esta clase?

- Cadenas de texto :: Veremos como trabajar y manipular cadenas de textos o
     *strings*, extraer información útil de ellas y ocupar las principales
     funcionalidades

- Tiempo y fechas :: Veremos el uso de fechas y tiempo, sus distintos formatos y
     como trabajarlos de manera integrada.

- Transformaciones y formatos :: Estudiaremos el uso de distintas herramientas
     para manipular las cadenas de texto (*strings*).

* Registrar inicio de clases
** Una marca en la línea de comandos: con nombre de clase

Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *strings_date*.

#+BEGIN_SRC bash
echo "CLASE_11::strings_date_03"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_11::strings_date_03*

** Obtener la posición de un substring dentro de una cadena

Es posible obtener el índice de la posición de una coincidencia dentro de una
cadena o string.

Para eso es necesario conocer la siguiente estructura.

#+BEGIN_SRC bash
buscar="mima"
msg="Mi mamá me mima"
recorte=${msg/$buscar*/}
posicion=${#recorte}
echo "La palabra $buscar está en la posición $posicion"
#+END_SRC

** Calculo de un valor medio, o la mitad de la frase

La mitad de la frase se calcularía así:

#+BEGIN_SRC bash
largo=${#msg}
# usando bc
mitad=$(echo $largo/2|bc)
# o bien 
mitad=$(($largo/2))
#+END_SRC

En cambio, con la opción *-l* de ~bc~, el valor que entrega es con decimales (o
punto flotante)

#+BEGIN_SRC bash
echo 5/2|bc -l
#+END_SRC


** Ejercicio: Mostrar caracteres de una frase

Hacer un script que solicite una frase al usuario, y muestre las siguientes
posiciones:

- primera
- mitad
- última

Además, que solicite una palabra a buscar y entregue la ~posición~ de donde se
encuentra la palabra.

Tomar nota, que, de manera por defecto, una operación divisoria con *bc* entrega
el valor *entero*.

*** Pendiente:

En la forma : (valor_decimal, valor_hexadecimal, caracter_minuscula, caracter_mayuscula)



** Eliminar coincidencia al inicio


En general *${variable#patron}*, que elimina la primera coincidencia del patrón
en la variable.
Analogo al caso anterior, para encontrar coincidencias al inicio usamos el siguiente
comando =${variable#patron}=. Nuevamente usar doble gato encontrará la coincidencia 
más larga.

#+BEGIN_SRC bash
#!/bin/bash
string=abcABC123ABCabc
echo ${string#abc}
#+END_SRC

#+BEGIN_SRC bash
myfile="/usr/share/icons/hicolor/64x64/mimetypes/application-wireshark-doc.png"
echo ${myfile##*/}
#+END_SRC
   
  - Hacer búsqueda y match con regex sobre el archivo ejercicios/{nombre}

** Eliminar coincidencia al final

Si usamos signo de porcentaje *${variable%patron}*,
encontraremos la última coincidencia. 


#+BEGIN_SRC bash
#!/bin/bash
string=abcABC123ABCabc
echo ${string%abc}
#+END_SRC

#+BEGIN_SRC bash
myfile="/home/david/Imágenes/imagen.png"
echo ${myfile%/*}
#+END_SRC

** Una introducción a la iteración

El uso de la estructura de control ~for~ nos permitirá leer una lista de
elementos y realizar una acción sobre cada uno de manera independiente. A esto
se le llama *iterar*

Por ejemplo, si se tiene una lista de ~animales~, iterar con ~for~ sería:

#+BEGIN_SRC bash
animales=(gato perro pulpo abeja copo lloica chinchilla)
# iteramos sobre la lista animales
for animal in ${animales[@]};
do
echo "El $animal es muy tierno"
done
#+END_SRC

También, en un caso de listar archivos:

#+BEGIN_SRC bash
# iteramos sobre la lista animales
carpeta="animales"
cd $carpeta
for archivo in $(ls);
do
echo "Este archivo de nombre $archivo está en $carpeta"
done
#+END_SRC

** Ejercicio: Eliminar al principio y al final.

Escribir un programa que haga lo siguiente:

- En el proyecto *selección literaria*
- buscar en el árbol de directorios una extensión  (uso de ~find~)
- eliminar de cada ruta entregada el nombre del archivo (eliminar coincidencia
  al final)
- mostrar las carpetas en que se encuentra cada archivo 
- buscar una palabra o frase específica en cada archivo con esa extensión
  (solicitar la palabra o frase, uso de ~find~, ~grep~ y ~xargs~)
- mostrar el nombre del archivo en que se tenga coincidencia.
- contar la cantidad de archivos que tengan esa extensión y ese contenido (uso
  de ~wc~)

* Fechas y tiempo

** El comando ~date~ para gestionar fecha y tiempo 

Esta sección está dedicada a trabajar especialmente con el comando *date* a
manera de poder obtener los mejores usos de los recursos que entrega.  

#+BEGIN_SRC bash
man date
#+END_SRC

Luego, podemos ver que hace:

1. entrega fecha y tiempo en el formato deseado

2. transforma fechas-tiempo en distintos formatos

3. permite operar entre fechas

** Añadimos localización para tener fechas en español

Editar el archivo *.bashrc*

Agregaremos la línea que permite tener las fechas con nombres de meses y días en
*castellano*. 

#+BEGIN_SRC bash
echo "export LC_TIME=es_ES">>~/.bashrc
#+END_SRC

** Obtener fecha y tiempo ahora

Escribir en la terminal

#+BEGIN_SRC bash
date
#+END_SRC

O bien, cargándola a una variable:

#+BEGIN_SRC bash
fecha=$(date)
echo "La fecha-tiempo de ahora es ${fecha}"
#+END_SRC

** La tabla de formatos

La tabla completa de formatos está disponible en el manual
#
|------------------+----------------------------------------------------------------|
| Carácter Formato | Descripción (en inglés)                                        |
|------------------+----------------------------------------------------------------|
| %D               | date; same as %m/%d/%y                                         |
| %F               | full date; same as %Y-%m-%d                                    |
| %H               | hour (00..23)                                                  |
| %I               | hour (01..12)                                                  |
| %j               | day of year (001..366)                                         |
| %k               | hour, space padded ( 0..23); same as %_H                       |
| %l               | hour, space padded ( 1..12); same as %_I                       |
| %m               | month (01..12)                                                 |
| %M               | minute (00..59)                                                |
| %R               | 24-hour hour and minute; same as %H:%M                         |
| %S               | Segundos 00->59                                                |
| %s               | seconds since 1970-01-01 00:00:00 UTC                          |
| %y               | Año abreviado 00->99                                           |
| %Y               | Año (ej: 2019)                                                 |
|------------------+----------------------------------------------------------------|
** Obtener fecha y tiempo con otro formato
Existe una manera de hacer que una Fecha-Tiempo tenga una representación numérica,
esta permite realizar operaciones matemáticas sobre el tiempo como avanzar o retroceder
una cantidad específica de tiempo (segundos, horas, días, etc). Esta representación
tiene por nombre *timestamp* y se obtiene de la siguiente manera:

#+BEGIN_SRC bash
date +%s
#+END_SRC

El valor obtenido corresponde a la cantidad de segundos desde el *01 de enero de 1970 00:00:00 UTC*

** Tiempo Unix, UTC
El tiempo *UTC* corresponde al [[https://es.wikipedia.org/wiki/Tiempo_universal_coordinado][Tiempo Universal Coordinado]] o de referencia sobre el cual la hora 
en nuestro *huso horario* se calcula.

Para obtener el tiempo de ahora en valor *UTC* basta con poner la opción "-u" a date.

#+BEGIN_SRC bash
date -u
date -u +"%D %H:%M:%S"
#+END_SRC

** Operaciones matemáticas en el tiempo
Además, *date* permite realizar operaciones básicas de desplazamiento sobre el tiempo, restando 
o sumando {segundos, horas, días, meses, años}. Por ejemplo:

#+BEGIN_SRC bash
  date -u +"%D %H:%M:%S"  -d "+1 day"
  date -u +"%D %H:%M:%S"  -d "+1 hour"
  date -u +"%D %H:%M:%S"  -d "-1 year"
  date -d "-1 year"
#+END_SRC
** Formato ISO8601
En muchos casos es necesario enviar información de tiempo desde un punto a otro, la manera más 
completa y compacta de hacerlo es mediante el *string tiempo* definido por el estándar [[https://es.wikipedia.org/wiki/ISO_8601][ISO8601]]
que contiene la información suficiente para operar con efectividad.

Este valor, con *date* se obtiene de la siguiente manera.

#+BEGIN_SRC bash
date --iso-8601=seconds
#+END_SRC

Como se puede observar, contienen la información completa de fecha-tiempo y zona horaria.

** Transformar tiempos en distintos formatos
Si quiero obtener la fecha de alguna forma especial se pueden utilizar expresiones
que combinen de distintas maneras los caracteres de la tabla de formato.

#+BEGIN_SRC bash
  date +%y
  date +%M%y
  date +%m%y
  date +%B%y
  date +%B %Y
  date +"%B %Y"
  date +"%d %B %Y"
  date +"+A %d %B %Y"
  date +"%A %d %B %Y"
  date +"%A %d %B, %Y"
  date +"%D"
  date +"%A %d %B, %Y %H:%m"
  date +"%A %d %B, %Y %H:%M:%s"
#+END_SRC

** Una fecha descrita a un formato

Pasar una fecha descrita a un formato puede ser sencillo, aunque a veces el formato de entrada
puede no ser aceptado. En este último caso será necesario el uso de otras herramientas para ajustar
los valores de entrada.

#+BEGIN_SRC bash
date -d "05 Oct 2010 07:38:40 +0400" +%F
#+END_SRC

Dependiendo del caso, tendremos que ocupar *sed* (que ya conocemos como opera) o
bien, *awk* que lo veremos en el siguiente capítulo.

** Ejercicio básico

¿Cómo pasar esta fecha 2018/05/25 (Y/m/d) o 2018/25/05 (Y/d/m), 20150525 a
 timestamp? [[https://stackoverflow.com/questions/14322614/bash-date-not-recognising-uk-format-dd-mm-yyyy][Ejemplo]]

En resumen, será de mucha ayuda usar la opción *-f*, para definir el formato de entrada.

** Ejercicio realizando script

Se necesita mostrar la fecha en distintos formatos, sin embargo el usuario debe
escoger el formato.

Escribir un script que solicite el formato según la tabla de ~date~ y mostrar la
fecha en ese formato, en el formato normal y con iso8601 para que se compare la
visualización.

